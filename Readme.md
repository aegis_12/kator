### Documentacion ###

Kator is used to do jobs massively. It will be applied to check web domains.

It will take the file called startups.txt with startup names and It will check if there exists .com domains for those names.

### Implementation ###

SpoutSlice is a interface which returns a channel of values to be checked. Right now, the channel is populated with the values from the file. On the future, It is intended to use an AWS queue.