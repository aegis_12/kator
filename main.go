package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/likexian/whois-go"
	"github.com/likexian/whois-parser-go"
)

//	Descargar una url
func downloadURL(domain string) (string, error) {
	resp, err := http.Get(domain)
	if err != nil {
		return "", err
	}

	//	Comprobar el status code (solo 200 valido)
	if resp.StatusCode != 200 {
		return "", errors.New("Invalid StatusCode " + resp.Status)
	}

	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

//  Peticion whois al dominio
func whoisPetition(domain string) (*whois_parser.WhoisInfo, error) {
	whoisResult, err := whois.Whois(domain)
	if err != nil {
		return nil, err
	}

	whoisParsed, err := whois_parser.Parser(whoisResult)
	if err != nil {
		return nil, err
	}

	return &whoisParsed, nil
}

//  Concurrent process to check url
func fetchDomainProcess(id int, domains chan string) {
	for {
		domain := <-domains
		_, err := downloadURL("http://www." + domain + ".com")
		if err != nil {
			fmt.Println(domain + " not found")
		} else {
			fmt.Println(domain + " found")
		}
	}
}

//  Spout que ofrece los resultados a partir de un txt
func spoutSlice(domains chan string, filename string) {
	dataB, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	dataS := string(dataB)
	items := strings.Split(dataS, "\n")

	for indx, item := range items {
		fmt.Printf("%d | %d\n", indx, len(items))
		domains <- item
	}
}

func main() {
	fmt.Println("-- Start Crawling --")

	//  C0ncurrent processes to fetch
	numProcess := 10

	//	Numero de cores
	//	runtime.GOMAXPROCS(4)

	//	Canales
	domains := make(chan string, numProcess*2)

	//	Crear los procesos concurrentes
	for i := 0; i < numProcess; i++ {
		go fetchDomainProcess(i, domains)
	}

	//	Proceso spout para las url (se usa como principal)
	spoutSlice(domains, "./startups.txt")

}

/**
	- Tiempos (comprobar 1000 dominios) -

---------------------------------
|	Procesos	|	Tiempo (s)	|
|				|				|
|				|				|
|				|				|
---------------------------------

*/
